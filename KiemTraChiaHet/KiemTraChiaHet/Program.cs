﻿using System;

class Program
{
    static void Main()
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        Console.Write("Nhập một số nguyên: ");
        int number = int.Parse(Console.ReadLine());

        if (CheckNumber(number))
        {
            Console.WriteLine($"Số {number} chia hết cho cả 2 và 3.");
        }
        else
        {
            Console.WriteLine($"Số {number} không chia hết cho cả 2 và 3.");
        }
    }

    static bool CheckNumber(int number)
    {
        return number % 2 == 0 && number % 3 == 0;
    }
}
